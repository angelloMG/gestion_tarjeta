import {LitElement, html} from 'lit-element';

class TipoOperacion extends LitElement {

    static get properties() {
        return {
            tipoOperacion: {type: String},
            showTipoOperacionForm: {type: Boolean}

        };
    }

    constructor() {
        super();
        this.tipoOperacion = "";
        this.showTipoOperacionForm = false;
        
    }

    render(){
        return html `

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

            <div id="TipoOperacion" @ok-screen="${this.showTipoOperacionForm()}" class="d-none" >
                <form>
                    <div class="form-group">
                        <div>
                            <label for="tipoOperacion">Operación</label>
                        </div>
                        <div>
                            <select @input="${this.updatedTipoOperacion}  name="tipoOperacion" id="tipoOperacion">
                                <option value="">Seleccione</option>
                                <option value="A">Bloqueo</option>
                                <option value="I">Desbloqueo</option>
                            </select>
                        </div>
                    </div>                  
                </form>    
                <div>
                    <button @click="${this.confirmarTipoOperacion} class="btn btn-primary><strong>Continuar</strong></button>
                </div>            
            </div>
            
        `;
    }


    updatedTipoOperacion(e){
        this.tipoOperacion = this.shadowRoot.querySelector('#tipoOperacion').value;
        console.log("Tipo de operación : " + this.tipoOperacion)

    }
    showTipoOperacionForm(e){

        console.log("confirmarTipoOperacion");
        this.shadowRoot.getElementById("TipoOperacion").classList.remove("d-none");    
        
    }

    



    confirmarTipoOperacion(e){
        console.log("confirmarTipoOperacion");
        e.preventDefault();
        if (this.tipoOperacion != ""){
                console.log("Tipo operacion : " + this.tipoOperacion);
                        
                this.dispatchEvent(new CustomEvent("tipo-operacion-confirmar", {
                    detail: {
                        //person: {
                            tipoOperacion: this.tipoOperacion,
                        //}
                    }
                }));
        }
        else {
            alert("Debe seleccionar una operación")
        }
        
    }

  
}

customElements.define('tipo-operacion',TipoOperacion)