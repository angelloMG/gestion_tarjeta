import {LitElement, html} from 'lit-element';

class ListaTarjetasItem extends LitElement {

    static get properties() {
        return {
            photo: {type: Object},
            cardNumber: {type: String},
            brand:  {type: String},
            statusCard: {type: String}
        };
    }

    constructor () {
        super();    
    }

    render(){
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <div class="card h-100">
                <img src="${this.photo.src}" alt="${this.photo.alt}" height="150" width="50" class="card-img-top"/>

                <div class="card-body">
                    <h5 class="card-title">${this.brand}</h5>
                    <p class="card-text">${this.cardNumber}</p>
                </div>

                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        status: ${this.statusCard}
                    </li>
                </ul>
                
                <div class="card-footer">
                <button @click="${this.cardDetail}" class="btn btn-info col-5 offset-1"><strong>Detalle Tarjeta</strong></button>
                </div>
            </div>

        `;
    }

    cardDetail(e) {
        console.log("cardDetail");
        console.log("entra al detalle de la tarjeta " + this.brand);

        this.dispatchEvent(
            new CustomEvent("detalle-tarjeta",{
                detail: {
                    cardNumber: this.cardNumber
                }
            })
        );
    }

}

customElements.define('lista-tarjetas-item',ListaTarjetasItem)