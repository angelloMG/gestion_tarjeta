import {LitElement, html} from 'lit-element';
import '../lista-tarjetas-item/lista-tarjetas-item.js'
import '../lista-tarjetas-detalle/lista-tarjetas-detalle.js'

class ListaTarjetas extends LitElement {

    static get properties() {
        return {
            tarjetas: {type: Array},
        };
    }

    constructor () {
        super();
        this.showCardDetail = false;
        this.tarjetas = [
            {
                "cardNumber": "1234567890123456",
                "brand": "MASTERCARD",
                "statusCard": "ACTIVA",
                "photo": {
                    "src": "./img/cardblocked.png",
                    "alt": "mastercard"
                }
            },
            {
                "cardNumber": "1239956960123456",
                "brand": "VISA",
                "statusCard": "ACTIVA",
                "photo": {
                    "src": "./img/visa.png",
                    "alt": "visa"
                }
            },
            {
                "cardNumber": "7899967890123456",
                "brand": "MASTERCARD",
                "statusCard": "ACTIVA",
                "photo": {
                    "src": "./img/mastercard.png",
                    "alt": "mastercard"
                }
            },
            {
                "cardNumber": "1234567890123456",
                "brand": "MASTERCARD",
                "statusCard": "ACTIVA",
                "photo": {
                    "src": "./img/mastercard.png",
                    "alt": "mastercard"
                }
            },
            {
                "cardNumber": "1234567890123456",
                "brand": "VISA",
                "statusCard": "ACTIVA",
                "photo": {
                    "src": "./img/visa.png",
                    "alt": "visa"
                }
            }
        ];
    }

    render(){
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <h2 class="text-center">Tarjetas</h2>

            <main>
                <div class="row" id="cardsList">
                    <div class="row row-cols-1 row-cols-sm-4">
                        ${this.tarjetas.map(
                            card => html`<lista-tarjetas-item cardNumber="${card.cardNumber}" brand="${card.brand}" statusCard="${card.statusCard}" .photo="${card.photo}" @detalle-tarjeta="${this.infoCard}"></lista-tarjetas-item>`
                        )}                
                    </div>
                </div>
                <div class="row">
                <lista-tarjetas-detalle id="listaTarjetasDetalle" class="d-none border rounded border-primary" @lista-tarjetas-detalle-close="${this.tarjetasDetailClose}" "></lista-tarjetas-detalle>
                </div>
            </main>
        `;
    }

    
    infoCard(e) {
        console.log("detalle tarjeta .. ");
        console.log("Se ha solicitado más detalle de tarjeta " + e.detail.cardNumber);

        let chosenCard = this.tarjetas.filter(
            tarjeta => tarjeta.cardNumber === e.detail.cardNumber
        );

        //this.shadowRoot.getElementById("peopleList").classList.add("d-none");	
        //this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("listaTarjetasDetalle").tarjeta = chosenCard[0];

        this.showCardDetail = true;

    }

    showCardsList() {
        console.log("showCardsList");
        console.log("Mostrando Listado de Tarjetas");
        this.shadowRoot.getElementById("cardsList").classList.remove("d-none");
        this.shadowRoot.getElementById("listaTarjetasDetalle").classList.add("d-none");
    }

    showCardDetailForm() {
        console.log("showCardDetailForm");
        console.log("Mostrando detalle de tarjeta");
        this.shadowRoot.getElementById("listaTarjetasDetalle").classList.remove("d-none");	  
        this.shadowRoot.getElementById("cardsList").classList.add("d-none");	
    } 

    tarjetasDetailClose() {
        console.log("tarjetasDetailClose");
        console.log("Se ha cerrado el detalle de la tarjeta");
        this.showCardDetail = false;
    }

    updated(changedProperties){
        console.log("updated");
        if (changedProperties.has("showCardDetail")) {
            console.log("Ha cambiado el valor de showCardDetail en lista-tarjetas");
            if (this.showCardDetail === true){
                this.showCardDetailForm();
            } else {
                this.showCardsList();
            }
        }

    }
}

customElements.define('lista-tarjetas',ListaTarjetas)