import {LitElement, html} from 'lit-element';

class LoginCliente extends LitElement {

    static get properties() {
        return {
            userData: {type: Object},
            showTipoOperacion: {type: Boolean},
            respuestaServicio: {type: Object}
        };
    }

    constructor() {
        super();
        this.showTipoOperacion = false;
        this.respuestaServicio = {};
        this.userData = {};
        this.userData.typeDocument = "";
        this.userData.idDocument = "";
        this.userData.password = "";
    
    }

    render(){
        return html `
            <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
            <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
            <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

            <style>
                /* Google Fonts */
                @import url(https://fonts.googleapis.com/css?family=Anonymous+Pro);

                /* Global */
                html{
                min-height: 100%;
                overflow: hidden;
                }
                body{
                height: calc(100vh - 8em);
                padding: 4em;
                color: rgba(255,255,255,.75);
                font-family: 'Anonymous Pro', monospace;  
                background-color: rgb(25,25,25);  
                }
                .line-1{
                position: relative;
                top: 50%;  
                width: 30em;
                margin: 0 auto;
                border-right: 2px solid rgba(255,255,255,.75);
                font-size: 100%;
                text-align: center;
                white-space: nowrap;
                overflow: hidden;
                transform: translateY(-50%);
                line-height: 25px;
                }

                /* Animation */
                .anim-typewriter{
                        animation: typewriter 2s steps(44) 1s 1 normal both;
                }

                @keyframes typewriter{
                from{width: 0;}
                to{width: 30em;}
                } 

                .anim-loginbox{
                    animation: fadeIn 1s ease-in both;
                    animation-delay: 7s;
                }

                @keyframes fadeIn {
                    0% {opacity:0;}
                    100% {opacity:1;}
                }

                .delay-animation3s{
                    animation-delay: 3s;
                }

                .delay-animation5s{
                    animation-delay: 5s;
                }

            </style>

            <br/>
            <div id="formAngello">
                <p id="Label1" class="line-1 anim-typewriter">Hola, bienvenido.</p>
                <p id="Label2" class="line-1 anim-typewriter delay-animation3s">Por aqui podrás bloquear o desbloquear tus tarjetas.</p>
                <p id="Label3" class="line-1 anim-typewriter delay-animation5s">Para ayudarte, ingresa tus datos de acceso a Canales Digitales</p>
                <p id="Label4" class="line-1 anim-loginbox">--------------------------------------------------------------------</p>
                <br/>
                <br/>
                <br/>
                <form class="line-1 anim-loginbox" >
                    <div>
                        <label>Tipo de Documento: </label>
                        <select @input="${this.updateDocType}" .value="${this.typeDocument}" placeholder="Ingrese su tipo de Documento">
                            <option value="DNI">DNI</option>
                            <option value="Carnet de Extranjería">Carnet de Extranjería</option>
                            <option value="Carnet Diplomático">Carnet Diplomático</option>
                            <option value="Partida de Nacimiento">Partida de Nacimiento</option>
                            <option value="Pasaporte">Pasaporte</option>
                            <option value="RUC">RUC</option>
                            <option value="Carnet Militar">Carnet Militar</option>
                        </select>
                    </div>
                    <div>
                        <label>Numero de Documento: </label>
                        <input @input="${this.updateDocNumber}" .value="${this.userData.idDocument}" type="text" placeholder="Ingrese su Número de Documento"/>
                    </div>
                    <div>
                        <label>Clave de acceso: </label>
                        <input @input="${this.updatePassword}" .value="${this.userData.password}" type="password" placeholder="Ingrese su Clave"/>
                    </div>
                    <div>
                        <button @click="${this.loginApp}" type="button" class="btn btn-success"><strong>Acceder</strong></button>
                    </div>
                </form>
                <p id="Label5" class="line-1 anim-loginbox">--------------------------------------------------------------------</p>
            </div>
        `;
    }

    updateDocType(e) {
        console.log("updateDocType");
        console.log("Actualizando la propiedad typeDocument con el valor " + e.target.value);
        this.userData.typeDocument = e.target.value;
    }

    updateDocNumber(e) {
        console.log("updateDocNumber");
        console.log("Actualizando la propiedad idDocument con el valor " + e.target.value);
        this.userData.idDocument = e.target.value;
    }

    updatePassword(e) {
        console.log("updatePassword");
        console.log("Actualizando la propiedad password con el valor " + e.target.value);
        this.userData.password = e.target.value;
    }

    loginApp(){
        console.log("loginApp");
        console.log(this.userData);

        fetch('http://localhost:8080/clientes', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.userData),
        })
        .then((response) => response.json())
        .then((data) => {
            console.log('Success:', data);
            this.respuestaServicio = data;
            this.okScreen();
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    }

    okScreen(){
        console.log("okScreen");
        console.log("Cambio de Pantalla Conforme");
        this.shadowRoot.getElementById("formAngello").classList.add("d-none");
        this.showTipoOperacion = true;
        this.dispatchEvent(new CustomEvent("ok-screen",{
            detail: {
                docType: this.userData.typeDocument,
                docNumber: this.userData.idDocument,
                fullName: this.respuestaServicio.fullName,
                showTipoOperacion: this.showTipoOperacion,
            }
        }));
    }
}

customElements.define('login-cliente',LoginCliente)